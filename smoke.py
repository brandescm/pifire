

import common as settings
import time
import queue
from cook_modes import Modes

class Smoke:

    def __init__(self, control, grill_platform, control_queue):
      self.control_queue = control_queue
    #   self.error_queue = error_queue
      self.grill_platform = grill_platform
      self.cook_state = Modes()
      self.settings = settings.ReadSettings()
      self.control = control
      self.on_time = self.settings['cycle_data']['SmokeCycleTime'] #  Auger On Time (Default 15s)
      self.off_time = 45 + (self.settings['cycle_data']['PMode'] * 10) 	#  Auger Off Time
      self.cycle_time = self.on_time + self.off_time 	#  Total Cycle Time
      self.duty_cycle = self.on_time / self.off_time #  Ratio of OnTime to CycleTime
      self.safe_band = 50
      self.set_point = 180
      self.control['setpoints']['grill'] = 180
      self.grill_platform.FanOn()
      self.grill_platform.IgniterOff()
      self.grill_platform.AugerOn()
      self.grill_platform.PowerOn()
      self.start_temp = self.control['temp']['grill']


    def run(self):
        elapse_now = time.time()
        output_now = elapse_now
        output_time = 0
        output_state = self.grill_platform.auger.is_active
        error = False
        # smoke set point is 160 - 180 anything above that
        # switch to cook and hold
    
        while not error:
            loop_time = time.time()
            output_time = loop_time - output_now
            
            if output_state == True:
                # print("Auger On")
                if output_time > self.on_time:
                    self.grill_platform.AugerOff()
                    self.control['outputs']['auger'] = 0 
                    output_now = time.time()
                    output_time = 0
                    output_state = self.grill_platform.auger.is_active
            if output_state == False:
                # print("Auger Off")
                if output_time > self.off_time:
                    self.grill_platform.AugerOn()
                    self.control['outputs']['auger'] = 1 
                    output_now = time.time()
                    output_time = 0
                    output_state = self.grill_platform.auger.is_active
            # Check for set point change before changing states
            self.set_point = self.control['setpoints']['grill']
            if self.set_point > 180:
                self.control_queue.put((1, self.cook_state.cook))
                return
            if self.set_point < 180:
                self.control_queue.put((1, self.cook_state.shutdown))
                return
