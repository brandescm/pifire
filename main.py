#!/usr/bin/env python3

import time
import os
import json
import datetime
from typing import Tuple
from common import *  # Common Library for WebUI and Control Program
# from pushbullet import Pushbullet # Pushbullet Import
import pid as PID # Library for calculating PID setpoints
# import requests
from threading import Thread
import queue
from enum import Enum

# from pifire.modes import Modes
from cook_modes import Modes
from ignite import Ignite
from cook_hold import Cook_and_Hold 
from smoke import Smoke 

class Main():
  
    def __init__(self) -> None:
        self.control_queue = queue.Queue(100)
        self.settings = ReadSettings()
        os.system('rm /home/pi/tmp/control.json')
        self.control = ReadControl()
        self.pellet_db = ReadPelletDB()
        self.debug = self.settings['globals']['debug_mode']
        self.grill0type = self.settings['probe_types']['grill0type']
        self.probe1type = self.settings['probe_types']['probe1type']
        self.probe2type = self.settings['probe_types']['probe2type']
        self.grill_platform = None
        self.display_device = None
        self.adc_device = None
        self.hopper_device = None
        self.set_point = self.control['setpoints']['grill']
        self.pid = None
        self.on_time = 0
        self.off_time = 0
        self.cycle_time = 0
        self.duty_cycle = 0
        self.error = None
        # Threads
        self.ignite_thread = None
        self.cook_thread = None
        self.app_thread = None
        self.queue_thread = None

        self.powered_on = False
        self.cook_state = Modes()
        # self.mode = self.cook_state.off
        self.last_mode = self.cook_state.off
        
        self._init_grill()
        self._init_ui()
        self._init_adc()
        # self._init_hopper()
        self.ignite = Ignite(self.control, self.grill_platform, self.control_queue)
        self.cook = Cook_and_Hold(self.control, self.grill_platform, self.control_queue)
        self.smoke = Smoke(self.control, self.grill_platform, self.control_queue)

        WriteLog('Control Script Starting Up.')
        # self._thread_start()

    def _init_hopper(self):
        if(self.settings['modules']['dist'] == 'vl53l0x'):
          from distance_vl53l0x import HopperLevel # Library for reading the HopperLevel from vl53l0x TOF Sensor
        elif(self.settings['modules']['dist'] == 'hcsr04'):
          from distance_hcsr04 import HopperLevel # Library for reading HopperLevel HC-SR04 Ultrasonic Sensor
        else: 
          from distance_prototype import HopperLevel # Simulated Library for reading the HopperLevel

        self.hopper_device = HopperLevel(self.pellet_db['empty'])
        # Get current hopper level and save it to the current pellet information
        self.pelletdb['current']['hopper_level'] = self.hopper_device.GetLevel()
        WritePelletDB(self.pelletdb)
        if self.debug:
            event = f"* Hopper Level Checked {self.pelletdb['current']['hopper_level']}%"
            print(event)
            WriteLog(event)
      
    def _init_grill(self):
        if(self.settings['modules']['grillplat'] == 'pifire'):
          from grillplat_pifire import GrillPlatform # Library for controlling the grill platform w/Raspberry Pi GPIOs
        else:
          from grillplat_prototype import GrillPlatform # Simulated Library for controlling the grill platform
        
        self.grill_platform = GrillPlatform(self.settings['outpins'], self.settings['inpins'], self.control_queue, self.settings['globals']['triggerlevel'])
        self.grill_platform.FanOff()
        self.grill_platform.IgniterOff()
        self.grill_platform.AugerOff()
        self.grill_platform.PowerOff()
        
    def _init_ui(self):
        # Start display device object and display splash
        if(self.settings['modules']['display'] == 'ssd1306'):
          from display_ssd1306 import Display # Library for controlling the display device
        elif(self.settings['modules']['display'] == 'ssd1306b'):
          from display_ssd1306b import Display # Library for controlling the display device w/button input
        elif(self.settings['modules']['display'] == 'st7789p'):
          from display_st7789p import Display # Library for controlling the display device
        elif(self.settings['modules']['display'] == 'pygame'):
          from display_pygame import Display # Library for controlling the display device
        elif(self.settings['modules']['display'] == 'pygame_240x320'):
          from display_pygame_240x320 import Display # Library for controlling the display device
        elif(self.settings['modules']['display'] == 'pygame_240x320b'):
          from display_pygame_240x320b import Display # Library for controlling the display device
        elif(self.settings['modules']['display'] == 'ili9341'):
          from display_ili9341 import Display # Library for controlling the display device
        elif(self.settings['modules']['display'] == 'ili9341b'):
          from display_ili9341b import Display # Library for controlling the display device
        else:
          from display_prototype import Display # Simulated Library for controlling the display device
        
        if(str(self.settings['modules']['display']).endswith('b')):	
          self.display_device = Display(self.button_level)
        else:
          self.display_device = Display(self.control_queue)
      
    def _init_adc(self):
        if(self.settings['modules']['adc'] == 'ads1115'):
            from adc_ads1115 import ReadADC # Library for reading the ADC device
        else: 
            from adc_prototype import ReadADC # Simulated Library for reading the ADC device
      
        self.adc_device = ReadADC(self.settings['probe_settings']['probe_profiles'][self.grill0type], 
                                  self.settings['probe_settings']['probe_profiles'][self.probe1type], 
                                  self.settings['probe_settings']['probe_profiles'][self.probe2type])


    def _thread_start(self):
        adc_thread = Thread(target=self._adc_loop)
        adc_thread.daemon = True
        display_thread = Thread(target=self._display_loop)
        display_thread.daemon = True
        display_thread.start()
        adc_thread.start()
        self.app_thread = Thread(target=self._app_loop)
        self.app_thread.daemon = True
        # self.app_thread.start()
        self.queue_thread = Thread(target=self._queue_loop)
        self.queue_thread.daemon = True
        self.queue_thread.start()

    def _queue_loop(self):
        priority = None
        msg = None
        self.set_point = self.control['setpoints']['grill']
        while True:
            priority, msg = self.control_queue.get()
            # print(f'message.....{msg}' )
            if priority == 0: # error occured
                self.error = msg
            elif priority == 1: # cook mode change
                self.control['mode'] = msg
            elif priority == 2: # encoder event
                if msg == 'cw':
                    self.set_point += 5
                    self.control['setpoints']['grill'] = self.set_point
                elif msg == 'ccw':
                    self.set_point -= 5
                    self.control['setpoints']['grill'] = self.set_point
                elif msg == 'pow':
                    if self.powered_on:
                        self.powered_on = False
                        self.control['mode'] = self.cook_state.shutdown
                    else:
                        self.powered_on = True 
            else:
                time.sleep(.05)
    # This is used for the history page for web interface
    # nothing else. Honestly a redundant way to do it. 
    def _app_loop(self):
        now = time.time()
        elapsed = 0
        while True:
            elapsed = time.time() - now
            if elapsed > 10:
                update_data = {}
                update_data['GrillTemp'] = self.control['temp']['grill']
                update_data['GrillSetPoint'] = self.control['setpoints']['grill']
                update_data['Probe1Temp'] = self.control['temp']['probe1']
                update_data['Probe1SetPoint'] = self.control['setpoints']['probe1']
                update_data['Probe2Temp'] = self.control['temp']['probe2']
                update_data['Probe2SetPoint'] = self.control['setpoints']['probe2']
                update_data['GrillTr'] = self.control['ohm']['grill']
                update_data['Probe1Tr'] = self.control['ohm']['probe1']
                update_data['Probe2Tr'] = self.control['ohm']['probe2']
                elapsed = 0
                now = time.time()
                WriteHistory(update_data)

    def _adc_loop(self):
        temp_data = {}
        count = 0
        grill_temp = 0
        probe_one = 0
        probe_two = 0
        grill_res = 0
        probe1_res = 0
        probe2_res = 0

        while True:
            count += 1
            if count < 10:
                temp_data = self.adc_device.ReadAllPorts()
                grill_temp += temp_data['GrillTemp']
                probe_one += temp_data['Probe1Temp']
                probe_two += temp_data['Probe2Temp']
                grill_res += temp_data['GrillTr']
                probe1_res += temp_data['Probe1Tr']
                probe2_res += temp_data['Probe2Tr']
                time.sleep(.1)
            else:
                grill_temp = grill_temp / 10
                probe_one = probe_one / 10
                probe_two = probe_two / 10
                self.control['temp']['grill'] = grill_temp
                self.control['temp']['probe1'] = probe_one
                self.control['temp']['probe2'] = probe_two
                self.control['ohm']['grill'] = grill_res
                self.control['ohm']['probe1'] = probe1_res
                self.control['ohm']['probe2'] = probe2_res
                grill_temp = 0
                probe_one = 0
                probe_two = 0
                grill_res = 0
                probe1_res = 0
                probe2_res = 0
                count = 0

    def _start_up(self):
        # self.on_time = self.settings['cycle_data']['SmokeCycleTime'] #  Auger On Time (Default 15s)
        # self.off_time = 45 + (self.settings['cycle_data']['PMode'] * 10) 	#  Auger Off Time
        # self.cycle_time = self.on_time + self.off_time 	#  Total Cycle Time
        # self.duty_cycle = self.on_time / self.off_time #  Ratio of OnTime to CycleTime
        # self.state_queue.put(states.States.str_state_to_index('Ignite'))
        time.sleep(2)
        self.control_queue.put((1, self.cook_state.ignite))


    def _ignite(self):
        self.ignite.run()
        # self.ignite_thread = Thread(target=self.ignite.run())
        # self.ignite_thread.daemon = True
        # self.ignite_thread.start()

    def _smoke(self):
        self.smoke.run()

    def _cook(self):
        self.cook.run()

    def _hold(self):
        self.cook.run()

    def _shutdown(self):
        self.on_time = 0 #  Auger On Time (Default 15s)
        self.off_time = 100 	#  Auger Off Time
        self.cycle_time = self.on_time + self.off_time 	#  Total Cycle Time
        self.duty_cycle = self.on_time / self.off_time #  Ratio of OnTime to CycleTime

    def _main_loop(self):
        mode = self.cook_state.off
        self._thread_start()

        while not self.powered_on:
            # self.powered_on = self.control_queue.get(timeout=1)
            time.sleep(.1)
        
        self.control_queue.put((1, self.cook_state.start_up))
        # hopper_check = self.control['hopper_check']
        while self.powered_on:
            mode = self.control['mode']
            if not self.last_mode == mode:
                self.last_mode = mode
                if mode == self.cook_state.start_up:
                    self._start_up()
                elif mode == self.cook_state.ignite:
                    if self.ignite_thread is not None:
                        pass # error already running
                    else:
                        self._ignite()
                elif mode == self.cook_state.smoke:
                    self._smoke()
                elif mode == self.cook_state.cook:
                    self._cook()
                elif mode == self.cook_state.hold:
                    self._hold()
                elif mode == self.cook_state.shutdown:
                    self._shutdown()
                elif mode == self.cook_state.retry:
                    pass
                elif mode == self.cook_state.error:
                    # display error and shutdown
                    self._shutdown()
                else:
                    mode = -1 #error unknown state
            else:  
                time.sleep(.1)

    def _display_loop(self):
        g_temp = 0
        g_sp = 0
        p1_temp = 0
        p1_sp = 0
        p2_temp = 0
        p2_sp = 0
        mode = self.control['mode']
        aug_state = 0
        fan_state = 0
        igniter_state = 0


        while True:
            g_temp = self.control['temp']['grill']
            g_sp = self.control['setpoints']['grill']
            if self.control['setpoints']['probe1'] > 0:
                p1_temp = self.control['temp']['probe1']
            else:
                p1_temp = 'N/A'
            aug_state = self.grill_platform.auger.is_active # move this to control
            fan_state = self.grill_platform.fan.is_active   # move this to control
            igniter_state = self.grill_platform.igniter.is_active # move this to control
            mode = self.cook_state.state_to_text(self.control['mode'])
            p2_temp = self.control['temp']['probe2']
            self.display_device.set_grill_temp(g_temp)
            self.display_device.set_grill_set_point(g_sp)
            self.display_device.set_probe_one_temp(p1_temp)
            self.display_device.set_probe_two_temp(p2_temp)
            self.display_device.set_mode(mode)
            self.display_device.set_auger_state(aug_state)
            self.display_device.set_fan_state(fan_state)
            self.display_device.set_igniter_state(igniter_state)

            self.display_device.DisplayStatus()
            time.sleep(.1)


def main():
  root = Main()
  root._main_loop()


if __name__ == '__main__':
    main()