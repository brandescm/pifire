import json
import os
from os.path import exists

class Control:

    def __init__(self):
        self.base_path = '/home/pi/tmp/'
        self.file_name = 'control.json'
        self.file_path = os.join(self.base_path, self.file_name)
        self.updated = False
        self.mode = 'Off'
        self.hopper_enabled = False
        self.recipe = None
        self.status = None
        self.probe_profile_update = False
        # Topic keys
        self.set_points = 'setpoints'
        self.temp = 'temp'
        self.ohm = 'ohm'
        self.notify_request = 'notify_req'
        self.notify = 'notify_data'
        self.manual_control = 'manual'
        self.output_state = 'outputs'

        self.defaults = {
            ['setpoints'] : {
                'grill': 0,
                'probe1': 0,
                'probe2': 0
            },
            ['temp'] : {
                'grill': 0,
                'probe1': 0,
                'probe2': 0
            },
            ['ohm'] : {
                'grill': 0,
                'probe1': 0,
                'probe2': 0
            },
            ['notify_req'] : {
                'grill': False,
                'probe1': False,
                'probe2': False,
                'timer': False,
            },
            ['notify_data'] : {
                'hopper_low': False,
                'p1_shutdown': False,
                'p2_shutdown': False,
                'timer_shutdown': False,
            },
            ['timer'] : {
                'start': 0,
                'paused': 0,
                'end': 0,
                'shutdown': False
            },
            ['manual'] : {
                'change': False,
                'output': '',
                'state': '',
                'current': {
                    'fan': 1,
                    'auger': 1,
                    'igniter': 1,
                    'power': 1
                }
            },
            ['outputs'] : {
                'fan': 0,
                'auger': 0,
                'igniter': 0
            },
        }
        
        if exists(self.file_path):
            with open(self.file_path) as json_file:
                self.control = json_file
        else:
            self.control = self.defaults.copy()
            self.write_control()

    def get_set_points(self):
        return self.control['set_points']
      
    def get_temp(self):
        return self.control['temp']
    
    def get_probe_resistance(self):
        return self.control['ohm']
      
    def get_notify_request(self):
        return self.control['notify_req']
      
    def get_notify_request(self):
        return self.control['notify_req']
      
    def get_notify_data(self):
        return self.control['notify_data']
      
    def get_timers(self):
        return self.control['timer']

    def get_output_states(self):
        return self.control['outputs']
    
    # *****************************************
    # Write all control states to JSON file
    # *****************************************
    def write_control(self):
        json_data_string = json.dumps(self.control)
        with open(self.file_path, 'w') as control_file:
          control_file.write(json_data_string)
          
    def get_control_defaults(self):
        copy = self.control
        return copy

    def default_control(self):
        self.control = self.defaults
        self.write_control

    # Set parameters in control dictionary
    # @input topic = String
    # @input data = dictionary of values to change 
    def set_settings(self, topic, data):
        for key, value in data.items():
            self.control[topic][key] = value
        
        self.write_control()