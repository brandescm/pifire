

import common as settings
import time
import queue
from cook_modes import Modes

class Ignite:

    def __init__(self, control, grill_platform, control_queue):
      self.control_queue = control_queue
    #   self.error_queue = error_queue
      self.grill_platform = grill_platform
      self.control = control
      self.cook_state = Modes()
      self.settings = settings.ReadSettings()
      
      self.on_time = self.settings['cycle_data']['SmokeCycleTime'] #  Auger On Time (Default 15s)
      self.off_time = 45 + (self.settings['cycle_data']['PMode'] * 10) 	#  Auger Off Time
      self.cycle_time = self.on_time + self.off_time 	#  Total Cycle Time
      self.duty_cycle = self.on_time / self.cycle_time #  Ratio of OnTime to CycleTime
      self.timeout = 1200
      self.retry = self.settings['safety']['reigniteretries']
      self.low_limit = self.settings['safety']['minstartuptemp']
      self.high_limit = self.settings['safety']['maxstartuptemp']
      self.grill_platform.FanOn()
      self.grill_platform.IgniterOn()
      self.grill_platform.AugerOn()
    #   self.grill_platform.PowerOn()
      self.start_temp = self.control['temp']['grill']
      self.blocking = True

    def run(self):
        elapse_now = time.time()
        output_now = elapse_now
        elapsed_time = 0
        output_time = 0
        output_state = self.grill_platform.auger.is_active
        retry_count = self.retry 
    
        while elapsed_time < self.timeout and retry_count != 0:
            loop_time = time.time()
            elapsed_time = loop_time - elapse_now
            output_time = loop_time - output_now
            
            if output_state == True:
                # print("Auger On")
                if output_time > self.on_time:
                    self.grill_platform.AugerOff()
                    self.control['outputs']['auger'] = 0 
                    output_now = time.time()
                    output_time = 0
                    output_state = self.grill_platform.auger.is_active
            if output_state == False:
                # print("Auger Off")
                if output_time > self.off_time:
                    self.grill_platform.AugerOn()
                    self.control['outputs']['auger'] = 1 
                    output_now = time.time()
                    output_time = 0
                    output_state = self.grill_platform.auger.is_active
            
            if self.control['mode'] != self.cook_state.ignite:
                # Some other event caused the mode to change
                return

            # Check temperature before changing states
            grill_temp = self.control['temp']['grill']
            if grill_temp > self.low_limit and grill_temp < self.high_limit:
                self.blocking = False
                self.control_queue.put((1, self.cook_state.smoke))
                return

            # if not self.blocking:
            #     if grill_temp > self.high_limit or grill_temp < self.low_limit:
            #         self.error_queue.put('Temp Error During Iginition')
            #         self.state_queue.put(self.cook_state.error)
            #         return

            if elapsed_time > self.timeout:
                print("Elapse time-out")
                elapsed_time = 0
                retry_count -= 1
                elapse_now = time.time()

            if retry_count == 0:
                print("Retry count is 0")
                self.control_queue.put((1, self.cook_state.error))
                return