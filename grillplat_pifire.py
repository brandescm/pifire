#!/usr/bin/env python3

# *****************************************
# PiFire OEM Interface Library
# *****************************************
#
# Description: This library supports
#  controlling the PiFire Outputs, alongside
#  the OEM controller outputs via
#  Raspberry Pi GPIOs, to a 4-channel relay
#
# *****************************************

# *****************************************
# Imported Libraries
# *****************************************

import RPi.GPIO as GPIO
from gpiozero import DigitalOutputDevice as relay
from gpiozero import Button as switch
from gpiozero import RotaryEncoder
import queue
import common as settings


class GrillPlatform:

		def __init__(self, outpins, inpins, control_queue, triggerlevel='LOW'):
				self.control_queue = control_queue
				# { 'power' : 4, 'auger' : 14, 'fan' : 15, 'igniter' : 18 }
				self.outpins = outpins
				self.inpins = inpins  # { 'selector' : 17 }
				self.active_high = True
				self.settings = settings.ReadSettings()
				if triggerlevel == 'LOW':
						# Defines for Active LOW relay
						self.RELAY_ON = 0
						self.RELAY_OFF = 1
				else:
						# Defines for Active HIGH relay
						self.active_high = False
						self.RELAY_ON = 1
						self.RELAY_OFF = 0
				# GPIOZERO setup =========================================
				#
				#=========================================================
				# Power Switch Setup 
				self.power_switch = switch(
						pin=17,
						# self.settings['inpins']['selector'],
						pull_up=True,
						# active_state=False,
						bounce_time=0.1,
						# hold_time=1,
						# hold_repeat=False
				)
				self.power_switch.when_activated = self._button_pressed

				# Encoder setup
				self.encoder = RotaryEncoder(21, 20,
																	bounce_time=.05,
																	max_steps=1,
																	wrap=True
																	)
				self.encoder.when_rotated_clockwise = self._encoder_cw
				self.encoder.when_rotated_counter_clockwise = self._encoder_ccw

				# Relay Output Setup
				self.auger = relay(
						self.settings['outpins']['auger'],
						self.active_high,
						initial_value=False,
						pin_factory=None
				)
				self.fan = relay(
						self.settings['outpins']['fan'],
						self.active_high,
						initial_value=False,
						pin_factory=None
				)
				self.igniter = relay(
						self.settings['outpins']['igniter'],
						self.active_high,
						initial_value=False,
						pin_factory=None
				)
				self.power = relay(
						self.settings['outpins']['power'],
						self.active_high,
						initial_value=False,
						pin_factory=None
				)
				# GPIO.setwarnings(False)
				# GPIO.setmode(GPIO.BCM)
				# for item in self.inpins:
				# 	GPIO.setup(self.inpins[item], GPIO.IN, pull_up_down=GPIO.PUD_UP)
				# 	GPIO.add_event_detect(self.inpins[item], GPIO.FALLING, callback=self._button_pressed)
				# if GPIO.input(self.inpins['selector']) == 0:
				# 	GPIO.setup(self.outpins['power'], GPIO.OUT, initial=self.RELAY_ON)
				# else:
				# 	GPIO.setup(self.outpins['power'], GPIO.OUT, initial=self.RELAY_OFF)
				# GPIO.setup(self.outpins['igniter'], GPIO.OUT, initial=self.RELAY_OFF)
				# GPIO.setup(self.outpins['fan'], GPIO.OUT, initial=self.RELAY_OFF)
				# GPIO.setup(self.outpins['auger'], GPIO.OUT, initial=self.RELAY_OFF)
				# self.pi_pwm = GPIO.PWM(self.outpins['auger'], 1000)
				# self.pi_pwm.start(0)

		def auger_duty_cycle_update(self, duty):
				self.pi_pwm.ChangeDutyCycle(duty)

		def auger_change_freq(self, freq):
				self.pi_pwm.ChangeFrequency(freq)

		def AugerOn(self):
				self.auger.on()
			# GPIO.output(self.outpins['auger'], self.RELAY_ON)

		def AugerOff(self):
				self.auger.off()
			# GPIO.output(self.outpins['auger'], self.RELAY_OFF)

		def FanOn(self):
				self.fan.on()
			# GPIO.output(self.outpins['fan'], self.RELAY_ON)

		def FanOff(self):
				self.fan.off()
			# GPIO.output(self.outpins['fan'], self.RELAY_OFF)

		def FanToggle(self):
				self.fan.toggle()
			# if(GPIO.input(self.outpins['fan']) == self.RELAY_ON):
			# 	GPIO.output(self.outpins['fan'], self.RELAY_OFF)
			# else:
			# 	GPIO.output(self.outpins['fan'], self.RELAY_ON)

		def IgniterOn(self):
				self.igniter.on()
			# GPIO.output(self.outpins['igniter'], self.RELAY_ON)

		def IgniterOff(self):
				self.auger.off()
			# GPIO.output(self.outpins['igniter'], self.RELAY_OFF)

		def PowerOn(self):
				self.power.on()
			# GPIO.output(self.outpins['power'], self.RELAY_ON)

		def PowerOff(self):
				self.power.off()
			# GPIO.output(self.outpins['power'], self.RELAY_OFF)

		def GetInputStatus(self):
				return self.power_switch.is_active
				# return (GPIO.input(self.inpins['selector']))

		def _button_pressed(self):
				self.control_queue.put((2, 'pow'))

		def _encoder_cw(self):
				self.control_queue.put((2, 'cw'))
		
		def _encoder_ccw(self):
				self.control_queue.put((2, 'ccw'))

		def GetOutputStatus(self):
				current = self.settings['outpins']
				current['power'] = self.power_switch.is_active
				current['auger'] = self.auger.is_active
				current['fan'] = self.fan.is_active
				current['igniter'] = self.igniter.is_active
				# for item in self.outpins:
				# 	current[item] = GPIO.input(self.outpins[item])
				return current
