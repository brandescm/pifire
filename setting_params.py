
import json
import os
from os.path import exists


class Settings:

    def __init__(self):
        self.base_path = '/home/pi/tmp/'
        self.file_name = 'settings.json'
        self.file_path = os.join(self.base_path, self.file_name)
        # Keys
        self.history = 'history'
        self.probe_enable = 'probe_enable'
        self.globals = 'globals'
        self.ifttt = 'ifttt'
        self.push_bullet = 'pushbullet'
        self.pushover = 'pushover'
        self.probe_types = 'probe_types'
        self.cycle_data = 'cycle_data'
        self.smoke_plus = 'smoke_plus'
        self.safety = 'safety'
        # Settings Dict
        self.defaults = {
          ['history']: {
              'minutes': 60,  # Sets default number of items to show in history
              'clearhistoryonstart': True,  # Clear history when StartUp Mode selected
              # Sets history graph to autorefresh ('live' graph)
              'autorefresh': 'on',
              'datapoints': 60  # Number of datapoints to show on the history chart
          },
          ['probe_enable']: {
              'grill_enable': True,
              'probe1_enable': False,
              'probe2_enable': False
          },
          ['globals']: {
              'grill_name': '',
              'debug_mode': True,
              'page_theme': 'light',
              'triggerlevel': 'LOW',
              'buttonslevel': 'HIGH',
              'shutdown_timer': 60,
              'units': 'F'
          },
          ['ifttt']: {
              'APIKey': '',  # API Key for WebMaker IFTTT App notification
          },
          ['pushbullet']: {
              'APIKey': '',  # API Key for Pushbullet notifications
              'PublicURL': '',  # Used in Pushbullet notifications
          },
          ['pushover']: {
              'APIKey': '',  # API Key for Pushover notifications
              'UserKeys': '',  # Comma-separated list of user keys
              'PublicURL': '',  # Used in Pushover notifications
          },
          ['probe_types']: {
              'grill0type': 'PT1000-00',
              'probe1type': 'TWPS00',
              'probe2type': 'TWPS00',
          },
          ['outpins']: {
              'power': 4,
              'auger': 16,
              'fan': 20,
              'igniter': 21
          },
          ['inpins']: {
              'selector': 17
          },
          ['cycle_data']: {
              'PB': 60.0,
              'Ti': 180.0,
              'Td': 45.0,
              'pid_u': 0.15,
              'HoldCycleTime': 20,
              'SmokeCycleTime': 15,
              'PMode': 2,  # http://tipsforbbq.com/Definition/Traeger-P-Setting
              'u_min': 0.15,
              'u_max': 1.0
          },
          ['smoke_plus']: {
              # Sets default Enable/Disable (True : Enabled, False : Disabled)
              'enabled': False,
              'min_temp': 160,  # Minimum temperature to cycle fan on/off
              'max_temp': 220,  # Maximum temperature to cycle fan on/off
              'cycle': 10,  # Number of seconds to cycle the fan on/off
              'frequency': 1,  # For PWM, if implemented (Currently not used)
              # For PWM, if implemented (Currently not used)
              'duty_cycle': 50
          },
          ['safety']: {
              # User Defined. Minimum temperature allowed for startup.
              'low_limit': 0,
              # User Defined. Take this value if the startup temp is higher than maxstartuptemp
              'high_limit': 550,
              # Number of tries to reignite the grill if it has gone below the safe temperature (set to 0 to disable)
              'reigniteretries': 1,
          },
          ['modules']: {
              # Grill Platform (PiFire - Raspberry Pi GPIOs)
              'grillplat': 'pifire',
              'adc': 'ads1115',			# Analog to Digital Converter Default is the ADS1115
              'display': 'ili9341',		# Default display is the SSD1306
              'dist': 'prototype'		# Default distance sensor is none
          }
        }

        if exists(self.file_path):
            with open(self.file_path) as json_file:
                self.settings = json_file
        else:
            self.settings = self.defaults.copy()
            self.write_settings()
    
    def get_settings(self):
        return self.settings

    # Returns list of safety params
    def get_safety_params(self):
        return self.settings['safety']
      
    def get_smoke_plus_params(self):
        return self.settings['smoke_plus']
      
    def get_cycle_params(self):
        return self.settings['cycle_data']
      
    def get_probe_params(self):
        return self.settings['probe_types']
      
    def get_global_params(self):
        return self.settings['globals']
      
    def get_probe_enable(self):
        return self.settings['probe_enable']
      
    def get_history_params(self):
        return self.settings['history']
    
    # Set parameters in settings dictionary
    # @input topic = String
    # @input data = dictionary of values to change 
    def set_settings(self, topic, data):
        for key, value in data.items():
            self.settings[topic][key] = value
        
        self.write_settings()
      
    # *****************************************
    # Write all settings to JSON file
    # *****************************************
    def write_settings(self):
        json_data_string = json.dumps(self.settings)
        with open("settings.json", 'w') as settings_file:
            settings_file.write(json_data_string)
