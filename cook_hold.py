# =============================================================================
# Cook and Hold Cycle
# Uses the PID loop for control
# Main loop runs wide open to update the output as needed based on
# the grill probe temperature. 
# Cook is defined as target temperature cooking, set point is used as the 
# target
# Hold is the cycle once cooking has completed and you are just keeping the
# product warm 

import common as settings
import time
import queue
from cook_modes import Modes
import pid as PID

class Cook_and_Hold:

    def __init__(self, control, grill_platfrom, control_queue):
        self.control_queue = control_queue
        # self.error_queue = error_queue
        self.grill_platform = grill_platfrom
        self.cook_state = Modes()
        self.settings = settings.ReadSettings()
        self.control = control
        self.min_duty_cycle = self.settings['cycle_data']['u_min'] 
        self.max_duty_cycle = self.settings['cycle_data']['u_max']
        self.cycle_time = self.settings['cycle_data']['HoldCycleTime']
        self.on_time = 3 
        self.off_time = 17	
        self.prop_band = self.settings['cycle_data']['PB']
        self.integral = self.settings['cycle_data']['Ti']
        self.derivative = self.settings['cycle_data']['Td']
        self.PIDControl = PID.PID(self.prop_band,self.integral,self.derivative)
        self.PIDControl.setTarget(self.control['setpoints']['grill'])	# Initialize with setpoint for grill
        self.low_limit = self.settings['safety']['minstartuptemp']
        self.high_limit = self.settings['safety']['maxtemp']
        self.set_point = 180
        self.control['setpoints']['grill'] = 180
        self.grill_platform.FanOn()
        self.grill_platform.IgniterOff()
        self.grill_platform.AugerOn()
        self.grill_platform.PowerOn()
        self.grill_temp = self.control['temp']['grill']
        self.blocking = True

    def run(self):
        elapse_now = time.time()
        output_now = elapse_now
        output_time = 0
        output_state = self.grill_platform.GetOutputStatus()
    
        while self.settings['mode'] is not self.cook_state.error:
            loop_time = time.time()
            # elapsed_time = loop_time - elapse_now
            output_time = loop_time - output_now
            # get temperature
            self.grill_temp = self.control['temp']['grill']
            duty_cycle = self.PIDControl.update(self.grill_temp)
            self.duty_cycle = max(duty_cycle, self.min_duty_cycle)
            self.duty_cycle = min(duty_cycle, self.max_duty_cycle)
            self.on_time = self.cycle_time * self.duty_cycle
            self.off_time = self.cycle_time - self.on_time
            
            if output_state == True:
                # print("Auger On")
                if output_time > self.on_time:
                    self.grill_platform.AugerOff()
                    self.control['outputs']['auger'] = 0 
                    output_now = time.time()
                    output_time = 0
                    output_state = self.grill_platform.auger.is_active
            if output_state == False:
                # print("Auger Off")
                if output_time > self.off_time:
                    self.grill_platform.AugerOn()
                    self.control['outputs']['auger'] = 1 
                    output_now = time.time()
                    output_time = 0
                    output_state = self.grill_platform.auger.is_active
            
            if self.grill_temp > self.low_limit and self.grill_temp < self.high_limit:
                self.blocking = False
            
            if not self.blocking:
                if self.grill_temp > self.high_limit:
                    self.error_queue.put('High Limit Error')
                    self.control_queue.put((1, self.cook_state.error))
                    return
                if self.grill_temp < self.low_limit:
                    self.error_queue.put('Low Limit Error')
                    self.control_queue.put((1, self.cook_state.error))
                    return
            time.sleep(.01)