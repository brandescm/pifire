#!/usr/bin/env python3


class Modes:
    def __init__(self):
        self.off = 0
        self.start_up = 1
        self.ignite = 2
        self.smoke = 3
        self.cook = 4
        self.hold = 5
        self.shutdown = 6
        self.retry = 7
        self.error = 8
        self.STATES = (
          'Off', 
          'Start Up', 
          'Ignite',
          'Smoke',
          'Cook',
          'Hold',
          'Shutdown',
          'Retry',
          'Error'
        )

    def state_to_text(self, value):
        return self.STATES[value]

    def str_state_to_index(self, str_value):
        return self.STATES.index(str_value)

    def get_states(self):
        return self.STATES

    def get_off(self):
        return self.off

    def get_startup(self):
        return self.start_up

    def get_ignite(self):
        return self.ignite

    def get_smoke(self):
        return self.smoke

    def get_cook(self):
        return self.cook
    
    def get_hold(self):
        return self.hold
    
    def get_shutdown(self):
        return self.shutdown
    
    def get_retry(self):
        return self.retry
    
    def get_error(self):
        return self.error